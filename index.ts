#!/usr/bin/env bun

/**
 * @file Handle main
 * @name index.ts
 * @license MIT
 */

import path from "node:path";
import fs from "node:fs";
import os from "node:os";

import GenerateTreeText from "contree";

const helpText = `\x1b[94;1mxecutable!\x1b[0m
    \x1b[93m--entry/-e\x1b[0m : specify entry file (Default: \x1b[91mnone\x1b[0m)
    \x1b[93m--out/-o\x1b[0m : specify output file (Default: \x1b[92mbundle.out\x1b[0m)`;

/**
 * @function ReadArgument
 *
 * @param {string} flag
 * @return {(string | undefined)}
 */
function ReadArgument(flag: string): string | undefined {
    const index = process.argv.indexOf(flag);
    if (index === -1) return undefined;
    return process.argv[index + 1];
}

// get entry file
const EntryFile = ReadArgument("--entry") || ReadArgument("-e");

// check entry file
if (
    !EntryFile ||
    !fs.existsSync(
        // if the EntryFile is an exact path, juse use that
        // otherwise we need to resolve the cwd and the EntryFile path
        path.isAbsolute(EntryFile)
            ? EntryFile
            : path.resolve(process.cwd(), EntryFile)
    )
) {
    console.error(helpText);
    process.exit(1);
}

// run build
const OutFile = ReadArgument("--output") || ReadArgument("-o") || "bundle.out";

// ...create out dir for build file
const OutDir = fs.mkdtempSync(path.join(os.tmpdir(), "build-"));

// ...build
await Bun.build({
    entrypoints: [EntryFile],
    outdir: OutDir,
    target: "bun",
    minify: true,
    splitting: true,
    format: "esm",
});

// ...build executable
Bun.spawn({
    cmd: [
        "bun",
        "build",
        ...[
            "--compile",
            `${OutDir}/${path.basename(EntryFile).replace(".ts", ".js")}`,
        ],
        ...["--outfile", OutFile],
        ...["--target", "bun"],
    ],
    onExit() {
        const fstat = fs.fstatSync(fs.openSync(OutFile, "r"));

        console.log(
            `\x1b[94;1mxecutable! \x1b[0;92mFinished build!\x1b[0m\n\n${GenerateTreeText(
                "Output File Information",
                [
                    [
                        "Size",
                        `Gigabytes: \x1b[93m${(fstat.size / 1000000000)
                            .toString()
                            .slice(0, 5)}GB\x1b[0m`,
                        `Megabytes: \x1b[93m${Math.floor(
                            fstat.size / 1000000
                        )}MB\x1b[0m`,
                        `Bytes: \x1b[93m${Math.floor(fstat.size)} Bytes\x1b[0m`,
                    ],
                    `Path: \x1b[93m${OutFile}\x1b[0m`,
                ]
            )}`
        );

        fs.rmSync(OutDir, { recursive: true });
        process.exit(0);
    },
});
