# xecutable

## Install

Requires the [Bun](https://bun.sh) runtime.

```
bun a -g xecutable
```

## Usage

```
xecutable!
    --entry/-e : specify entry file (Default: none)
    --out/-o : specify output file (Default: bundle.out)
```

Example:

```
xecutable --entry ./index.ts --out bundle.out
```
